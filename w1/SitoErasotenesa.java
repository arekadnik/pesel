package zprogramowanie_poziom_1.w1;

import java.time.chrono.Era;

public class SitoErasotenesa {
    public static void main(String[] args) {
        boolean[] erasito = Erastote(10);
        boolean isPrieme = erasito[5];


        System.out.println("isPrieme = " + isPrieme);

        for (boolean v : erasito) {
            System.out.println(v);
        }

    }

    public static boolean[] Erastote(int n) {

        boolean[] array = new boolean[n];
        for (int i = 0; i < array.length; i++) {

            array[i] = true;
        }

        array[0] = false;
        array[1] = false;

        for (int i = 2; i < n; i++) {
            if (array[i]) {
                for (int j = i +2; j<array.length ;j = j + i) {

                    array[j] = false;
                }

            }


        }
        return array;
    }

}
