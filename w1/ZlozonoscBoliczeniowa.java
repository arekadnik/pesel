package zprogramowanie_poziom_1.w1;



public class ZlozonoscBoliczeniowa {
    public static void main(String[] args) {


        int[] array = {1, 2, 3, 9, 8, 7, 6, 35, 24, 76, 43, 21, 23};


        bubbleSort(array);
        printArray(array);

    }

    public static void bubbleSort(int[] array) {

        int n = array.length;
        for (int i = 0; i < n - 1; i++)
            for (int j = 0; j < n - i - 1; j++)
                if (array[j] > array[j + 1]) {
                    // swap temp and arr[i]
                    int temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }

    }


    public static void printArray(int[] array) {

        for (int v : array) {
            System.out.print(v + " ");
        }
    }
}
