package zprogramowanie_poziom_1.w1;

import java.util.Scanner;

public class Spoi2 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int casesCount = scanner.nextInt();
        for (int i = 0; i < casesCount; i++) {
            int a = scanner.nextInt();
            int b = scanner.nextInt();
            System.out.println(getLastDigit(a, b));
        }
    }

    public static int getLastDigit(int c, int d) {

        int result = 1;
        for (int i = 0; i < d; i++) {
            result *= c;
        }
        System.out.println(result);
        return result % 10;
    }
}