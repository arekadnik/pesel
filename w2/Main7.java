package zprogramowanie_poziom_1.w2;

import java.util.Scanner;

public class Main7 {
    public static void main(String[] args) {

        int[] array = {2, 3, 5, 7, 10, 24, 34, 56, 78, 98};

        int min = 0;
        int max = array.length - 1;

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter number to find");
        int search = scanner.nextInt();

        while (min <= max) {
            int mid = (min + max) / 2;
            if (array[mid] == search) {
                System.out.println("Element found! " + search + " has been found at location " + (mid + 1) + ".");
                break;
            } else if (array[mid] < search) {
                min = mid + 1;
            } else if (array[mid] > search) {
                max = mid - 1;
            }
        }
        if (min >= max)
            System.out.println("-1, " + search + " is not present in the list.");

    }
}
