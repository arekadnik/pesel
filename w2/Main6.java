package zprogramowanie_poziom_1.w2;

public class Main6 {
    public static void main(String[] args) {
        int[] array1 = {9, 8, 7, 6, 5, 4, 3};
        int[] newArray = bubbleSort(array1);
        isNumberInArray(newArray, 10);


    }
    //dlacego nie printuje sie -1 kiedy daje liczbe z poza zakresu !!!

    public static int isNumberInArray(int[] array, int numberToSearch) {


        int k = 0;
        for (int v : array) {
            if (numberToSearch == v) {
                System.out.println("number to search was " + numberToSearch + '\n' + " index of number = " + k);
            }
            k++;
        }
        return -1;
    }

    public static int[] bubbleSort(int[] array) {
        int n = array.length;
        for (int i = 0; i < n - 1; i++)
            for (int j = 0; j < n - i - 1; j++)
                if (array[j] > array[j + 1]) {
                    // swap temp and arr[i]
                    int temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;

                }

        return array;
    }
}


