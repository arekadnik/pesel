package zprogramowanie_poziom_1.w2;

import java.util.*;

public class Main3 {
    public static void main(String[] args) {

        Map<String, Integer> ageMap = new HashMap<>();
        ageMap.put("Arek1", 20);
        ageMap.put("Arek2", 21);
        ageMap.put("Arek3", 22);
        ageMap.put("Arek4", 23);
        ageMap.put("Arek5", 18);
        ageMap.put("Arek6", 25);
        ageMap.put("Arek7", 26);
        ageMap.put("Arek8", 27);
        ageMap.put("Arek9", 28);
        ageMap.put("Arek10", 29);
        ageMap.put("Arek11", 29);
        printCollection(ageMap.values());
        printCollection(ageMap.keySet());
        printCollection(ageMap.entrySet());

        System.out.println("Kolejna linia");
        System.out.println();
        System.out.println();
        System.out.println();

        System.out.println( ageMap.containsKey("janusz"));
        System.out.println(ageMap.containsValue(18));

        if (ageMap.containsKey("Janusz")) {
            System.out.println("zawiera");
        } else {
            System.out.println("nie zawiera");
        }

        adult(ageMap);
        printCollection(ageMap.keySet());
        printCollection(ageMap.values());

    }

    private static void adult(Map<String, Integer> ageMap) {
        if (ageMap.containsValue(18)) {
            System.out.println("sa osoby w wieku 18 lat");
        } else {
            System.out.println("nie ma osob w wieku 18 lat");
        }
    }

    private static void printCollection(Collection collection) {
        for (Object element : collection) {
            System.out.println(element.toString());
        }
    }
}
