package zprogramowanie_poziom_1.w2;

import java.util.*;

public class Main2 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        Set<Integer> lottSet = new TreeSet<>();//mozna tez zrobic HashSet tylko liczby
        // nie bede w kolejnosci rosnacej

        do {
            try {
                System.out.println("podaj nowa liczbe");
                Integer a = scanner.nextInt();
                if (lottSet.contains(a)) {
                    System.out.println("podaj inna liczbe ");
                } else if (a < 6 || a > 49) {
                    System.out.println("podaj liczbe z zakresu 6-49");
                } else
                    lottSet.add(a);
            } catch (InputMismatchException exception) {
                System.out.println("Zlapalem wyjatek " + exception);
                scanner.next();
            }
        }
        while (lottSet.size() < 6);


        for (Integer v : lottSet) {
            System.out.println(v);
        }


    }
}
