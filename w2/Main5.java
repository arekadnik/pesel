package zprogramowanie_poziom_1.w2;

public class Main5 {
    public static void main(String[] args) {

        int[] array1 = {9, 8, 7, 6, 5, 4, 3};
        findNumber(array1, 5);

    }

    public static int findNumber(int[] array, int numberTosearch) {

        int n = array.length;
        for (int i = 0; i < n - 1; i++)
            for (int j = 0; j < n - i - 1; j++)
                if (array[j] > array[j + 1]) {
                    // swap temp and arr[i]
                    int temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
        int i = 0;
        try {
            do {
                if (array == null) {
                    System.out.println();
                } else if (numberTosearch == array[i]) {
                    System.out.println("numberTosearch " + numberTosearch + " index = " + i);
                } else {
                    System.out.println(array[i]);
                }
            } while (numberTosearch != array[i++]);
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println(e + "  number does not exist");
        }
        return -1;
    }

}









