package zprogramowanie_poziom_1.w3;

import java.util.Scanner;

public class Pesel {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        String pesel = scanner.nextLine();

        checkYourPesel(pesel);


    }

    private static void checkYourPesel(String pesel) {
        checkPesel(pesel);
        yearOfBirth(pesel);
        System.out.println(isYourPeselCorrect(pesel));
        yearOfBirth(pesel);
        getBirthMonth(pesel);
        getBirthDay(pesel);
        getSex(pesel);
    }

    public static void checkPesel(String pesel) {

        if (pesel.length() != 11) {
            System.out.println("nie poprawny Pesel");
        }
    }

    public static void yearOfBirth(String pesel) {

        int year;
        year = 10 * Character.getNumericValue(pesel.charAt(0));
        year += Character.getNumericValue(pesel.charAt(1));
        int month;
        month = 10 * Character.getNumericValue(pesel.charAt(2));
        month += Character.getNumericValue(pesel.charAt(3));
        if (month > 80 && month < 93) {
            year += 1800;
        } else if (month > 0 && month < 13) {
            year += 1900;
        } else if (month > 20 && month < 33) {
            year += 2000;
        } else if (month > 40 && month < 53) {
            year += 2100;
        } else if (month > 60 && month < 73) {
            year += 2200;
        }
        System.out.println(year);
    }


    public static boolean isYourPeselCorrect(String pesel) {

        int[] digitControl = {1, 3, 7, 9, 1, 3, 7, 9, 1, 3};

        int sum = 0;

        for (int i = 0; i < 10; i++) {
            sum += digitControl[i] * Integer.parseInt(pesel.substring(0, 9));
        }
        int cyfraKontrolna = Integer.parseInt(pesel.substring(10, 11));
        sum %= 10;
        sum = 10 - sum;
        sum %= 10;
        return (sum == cyfraKontrolna);
    }

    public static void getBirthMonth(String pesel) {
        int month;
        month = 10 * Character.getNumericValue(pesel.charAt(2));
        month += Character.getNumericValue(pesel.charAt(3));

        if (month > 80 && month < 93) {
            month -= 80;
        } else if (month > 20 && month < 33) {
            month -= 20;
        } else if (month > 40 && month < 53) {
            month -= 40;
        } else if (month > 60 && month < 73) {
            month -= 60;
        }
        System.out.println("your birth month is: " + month);
    }

    public static void getBirthDay(String pesel) {
        int day;
        day = 10 * Character.getNumericValue(pesel.charAt(4));
        day += Character.getNumericValue(pesel.charAt(5));
        System.out.println("you have birthday on " + day);
    }

    public static void getSex(String pesel) {
        if (Character.getNumericValue(pesel.charAt(9)) % 2 == 0) {
            System.out.println("female");
        } else {
            System.out.println("male");
        }
    }

}

