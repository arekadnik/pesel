package zprogramowanie_poziom_1.w3;

public class Main {
    public static void main(String[] args) {
        System.out.println(isContainString("dupa", "up"));


    }

    public static boolean isContainString(String mainString, String stringToSearch) {


        for (int i = 0; i < mainString.length(); i++) {
            if (mainString.charAt(i) == stringToSearch.charAt(0)) {
                int subIndex = 0;
                int stringIndex = i;
                while (mainString.charAt(stringIndex) == stringToSearch.charAt(subIndex)) {
                    subIndex++;
                    stringIndex++;
                    if (subIndex == stringToSearch.length()) {
                        return true;
                    }
                    if (stringIndex == mainString.length()){
                        return false;
                    }
                }
            }
        }
        return false;
    }

}






